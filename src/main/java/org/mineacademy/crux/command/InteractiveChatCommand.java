package org.mineacademy.crux.command;

import org.mineacademy.fo.ChatUtil;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.command.SimpleCommand;
import org.mineacademy.fo.model.SimpleComponent;

public class InteractiveChatCommand extends SimpleCommand {

	public InteractiveChatCommand() {
		super("interactivechat|ic");
	}

	@Override
	protected void onCommand() {
		checkConsole();

		SimpleComponent

				// TIP: You can use Common#chatLineSmooth and ChatUtil#center
				// also for basic tell() or Common#tell methods
				.of("&8" + Common.chatLineSmooth())
				.append(ChatUtil.center("Moderation Tools"))
				.append("&8" + Common.chatLineSmooth())

				.append("&6[Day]")
				.onHover("Click to set the day.")
				.onClickRunCmd("/time set day")
				.append(" &7- Set this world time to day.")

				.append("\n&9[Night]")
				.onHover("Click to set the night.")
				.onClickRunCmd("/time set night")
				.append(" &7- Set this world time to night.")

				.append("\n&c[Hand]")
				.onHover(getPlayer().getItemInHand())
				.append(" &7- View your hand item.")

				.append("\n&e[PM]")
				.onHover("Click to suggest a PM.")
				.onClickSuggestCmd("/tell " + sender.getName() + " Hello there!")
				.append(" &7- Suggest a PM")

				.append("\n&b[Website]")
				.onHover("Click to visit our site.")
				.onClickOpenUrl("https://mineacademy.org")
				.append(" &7- Open our home url.")

				.send(sender);


	}
}
