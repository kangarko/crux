package org.mineacademy.crux.command;

import java.util.concurrent.TimeUnit;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.mineacademy.fo.BlockUtil;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.RandomUtil;
import org.mineacademy.fo.command.SimpleCommand;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.remain.CompProperty;
import org.mineacademy.fo.remain.CompSound;

public final class RandomTeleportCommand extends SimpleCommand {

	private final static int LOCATION_SEARCH_TRIES = 10;

	private final static int DECREASE_HEIGHT_PER_ANIMATION = 80;

	public RandomTeleportCommand() {
		super("randomtp|random|wild|build");

		setTellPrefix("&8[&5RandomTp&8]&7");

		setCooldown(10, TimeUnit.SECONDS);
		setCooldownMessage("Please wait {duration} second(s) before teleporting again.");

		setMinArguments(1);
		setUsage("<range>");
	}

	@Override
	protected void onCommand() {
		checkConsole();

		final Player player = getPlayer();
		final int range = findNumber(0, 100, 10_000, "Please specify a whole number from {min} to {max}.");

		tell("A random location is being searched for...");

		final Location location = findLocation(new Location(player.getWorld(), 0, 0, 0), range);
		checkNotNull(location, "Could not find any suitable location, try again later.");

		if (!location.getChunk().isLoaded())
			location.getChunk().load(true);

		location.setYaw(0);
		location.setPitch(90);

		CompSound.SUCCESSFUL_HIT.play(player);
		player.playEffect(player.getLocation(), Effect.ENDER_SIGNAL, null);

		startTeleportAnimationTask(location);
	}

	private void startTeleportAnimationTask(final Location location) {
		Common.runTimer(20 * 2, 15 /*0.75 second*/, new BukkitRunnable() {

			private int currentHeight = location.getWorld().getMaxHeight();
			private boolean playerFrozen = false;
			private float previousWalkSpeed, previousFlySpeed;

			@Override
			public void run() {
				final Player player = getPlayer();

				if (!playerFrozen) {
					previousWalkSpeed = player.getWalkSpeed();
					previousFlySpeed = player.getFlySpeed();

					player.setWalkSpeed(0);
					player.setFlySpeed(0);

					player.setAllowFlight(true);
					player.setFlying(true);

					CompProperty.GRAVITY.apply(player, false);
					CompProperty.INVULNERABLE.apply(player, true);

					playerFrozen = true;
				}

				if (currentHeight <= DECREASE_HEIGHT_PER_ANIMATION) {
					cancel();

					final Location groundLocation = player.getLocation().clone().subtract(0, currentHeight, 0);
					groundLocation.add(0.5, 0, 0.5);
					groundLocation.setYaw(90);
					groundLocation.setPitch(0);

					player.teleport(groundLocation);

					player.setWalkSpeed(previousWalkSpeed);
					player.setFlySpeed(previousFlySpeed);

					player.setAllowFlight(false);
					player.setFlying(false);

					CompProperty.GRAVITY.apply(player, true);
					CompProperty.INVULNERABLE.apply(player, false);

					return;
				}

				currentHeight -= DECREASE_HEIGHT_PER_ANIMATION;

				player.teleport(location.clone().add(0, currentHeight, 0));

				player.playEffect(player.getLocation(), Effect.ENDER_SIGNAL, null);
				CompSound.ENDERDRAGON_WINGS.play(player);
			}
		});
	}

	private Location findLocation(final Location center, final int range) {

		for (int i /* try attempt number */ = 0; i < LOCATION_SEARCH_TRIES; ++i) {

			final Location location = RandomUtil.nextLocation(center, range, false);
			final int highestPointY = BlockUtil.findHighestBlock(location, material -> !CompMaterial.isLeaves(material));

			if (highestPointY != -1) {
				location.setY(highestPointY);

				final Block block = location.getBlock();
				final Block blockAbove = block.getRelative(BlockFace.UP);
				final Block blockBelow = block.getRelative(BlockFace.DOWN);

				if (blockBelow.getType().isSolid() && CompMaterial.isAir(block) && CompMaterial.isAir(blockAbove))
					return location;
			}
		}

		return null;
	}
}
