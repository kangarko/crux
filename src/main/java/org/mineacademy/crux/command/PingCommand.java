package org.mineacademy.crux.command;

import org.bukkit.entity.Player;
import org.mineacademy.fo.MinecraftVersion;
import org.mineacademy.fo.PlayerUtil;
import org.mineacademy.fo.command.SimpleCommand;

public final class PingCommand extends SimpleCommand {

	public PingCommand() {
		super("ping" );
	}

	@Override
	protected void onCommand() {
		checkConsole();

		final Player player = getPlayer();

		if (MinecraftVersion.atLeast(MinecraftVersion.V.v1_14))
			player.sendSignChange(player.getLocation(), new String[3]);
		else
			tell("Sending sign updates requires MC 1.14.3");

		// Way 1: Direct import
		//final CraftPlayer craftPlayer = (CraftPlayer) player;
		//tell("Your ping: " + craftPlayer.getHandle().ping + "ms");

		// Way 2: Classic reflection
		/*final Class<?> craftPlayerClass = ReflectionUtil.getOBCClass("entity.CraftPlayer");
		final Method getHandleMethod = ReflectionUtil.getMethod(craftPlayerClass, "getHandle");
		final Object entityPlayer = ReflectionUtil.invoke(getHandleMethod, getPlayer());
		final int ping = ReflectionUtil.getFieldContent(entityPlayer, "ping");
		tell("Your ping (reflection): " + ping);*/

		// Way 3: The enhanced way
		/*final Object entityPlayer = Remain.getHandleEntity(getPlayer());
		final int ping = ReflectionUtil.getFieldContent(entityPlayer, "ping");
		tell("Your ping (reflection V2): " + ping); */

		// Way 4: The next level way
		tell("Your ping (next level way): " + PlayerUtil.getPing(getPlayer()));
	}
}
