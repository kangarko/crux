package org.mineacademy.crux.command;

import org.bukkit.entity.Player;
import org.mineacademy.fo.command.SimpleCommand;
import org.mineacademy.fo.model.SimpleScoreboard;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.remain.Remain;

public class NotificationCommand extends SimpleCommand {

	public NotificationCommand() {
		super("notif");

		setMinArguments(2);
		setUsage("<tab|score|title|action|toast> <input|...>");
		// /notif title Hello World|Check out our website
	}

	@Override
	protected void onCommand() {
		checkConsole();

		final Player player = getPlayer();
		final String param = args[0].toLowerCase();

		final String[] inputs = joinArgs(1).split("\\|");
		final String primaryPart = inputs[0];
		final String secondaryPart = inputs.length == 1 ? "" : inputs[1];

		if ("tab".equals(param) || "title".equals(param)) {
			checkBoolean(inputs.length == 2, "Usage: /{label} {0} <primary>|<secondary>");

			if ("tab".equals(param))
				Remain.sendTablist(player, primaryPart, secondaryPart);
			else
				Remain.sendTitle(player, primaryPart, secondaryPart);
		}

		else if ("action".equals(param))
			Remain.sendActionBar(player, primaryPart);

		else if ("toast".equals(param))
			Remain.sendToast(player, primaryPart, CompMaterial.CAKE);

		else if ("score".equals(param)) {
			final SimpleScoreboard scoreboard = new MyScoreboard();

			scoreboard.setTitle("&cCrux Scoreboard");
			scoreboard.addRows((Object[]) inputs);

			// The higher this update rate (in ticks) the lower performance strain on your server
			// NB: Rapidly updating scoreboards may flash
			scoreboard.setUpdateDelayTicks(20);

			if (scoreboard.isViewing(player))
				scoreboard.hide(player);

			scoreboard.show(player);
		}
	}
}

class MyScoreboard extends SimpleScoreboard {

	/**
	 * @see org.mineacademy.fo.model.SimpleScoreboard#replaceVariables(org.bukkit.entity.Player, java.lang.String)
	 */
	@Override
	protected String replaceVariables(Player player, String message) {
		return message.replace("{online}", Remain.getOnlinePlayers().size() + "");
	}
}
