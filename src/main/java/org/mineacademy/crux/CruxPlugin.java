package org.mineacademy.crux;

import org.mineacademy.crux.command.InteractiveChatCommand;
import org.mineacademy.crux.command.NotificationCommand;
import org.mineacademy.crux.command.PingCommand;
import org.mineacademy.crux.command.RandomTeleportCommand;
import org.mineacademy.crux.event.BreakingArrowsListener;
import org.mineacademy.crux.event.CustomHealthListener;
import org.mineacademy.crux.event.FastFoodListener;
import org.mineacademy.crux.event.JumpBoostListener;
import org.mineacademy.crux.event.RespawnListener;
import org.mineacademy.crux.event.TreeGravityListener;
import org.mineacademy.crux.task.ElytraTask;
import org.mineacademy.fo.plugin.SimplePlugin;

public final class CruxPlugin extends SimplePlugin {

	@Override
	protected void onPluginStart() {
		registerCommand(new RandomTeleportCommand());
		registerCommand(new InteractiveChatCommand());
		registerCommand(new NotificationCommand());
		registerCommand(new PingCommand());

		registerEvents(new RespawnListener());
		registerEvents(new BreakingArrowsListener());
		registerEvents(new FastFoodListener());
		registerEvents(new CustomHealthListener());
		registerEvents(new JumpBoostListener());
		registerEvents(new TreeGravityListener());

		new ElytraTask().runTaskTimer(this, 0, 20);
	}

	/*@Override
	public MinecraftVersion.V getMinimumVersion() {
		return MinecraftVersion.V.v1_14;
	}*/

	@Override
	protected void onPluginStop() {
		getServer().getScheduler().cancelTasks(this);
	}
}
