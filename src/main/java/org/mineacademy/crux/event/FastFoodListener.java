package org.mineacademy.crux.event;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.MathUtil;
import org.mineacademy.fo.PlayerUtil;
import org.mineacademy.fo.ReflectionUtil;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.remain.CompSound;
import org.mineacademy.fo.remain.Remain;

public final class FastFoodListener implements Listener {

	@EventHandler // ignoreCancelled = false
	public void onClick(final PlayerInteractEvent event) {
		if (!Remain.isInteractEventPrimaryHand(event) || !event.getAction().toString().contains("RIGHT_CLICK"))
			return;

		final Player player = event.getPlayer();

		if (player.getGameMode() == GameMode.SURVIVAL) {
			final ItemStack hand = player.getItemInHand();
			final Food food = Food.getFood(hand);

			if (food != null && player.getHealth() < player.getMaxHealth()) {
				final double newHealth = MathUtil.range(player.getHealth() + food.getHealth(), 0, player.getMaxHealth());

				player.setHealth(newHealth);
				CompSound.EAT.play(player);

				PlayerUtil.takeOnePiece(player, hand);
				event.setCancelled(true);
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onFoodLevelChange(final FoodLevelChangeEvent event) {
		event.setFoodLevel(20);
	}

	@EventHandler
	public void onJoin(final PlayerJoinEvent event) {
		event.getPlayer().setFoodLevel(20);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onHealthChange(final EntityRegainHealthEvent event) {
		final EntityRegainHealthEvent.RegainReason reason = event.getRegainReason();

		if (reason == EntityRegainHealthEvent.RegainReason.REGEN || reason == EntityRegainHealthEvent.RegainReason.SATIATED)
			event.setCancelled(true);
	}
}

/**
 * A private enum to be placed beneath your class storing
 * how much health should each individual food give.
 *
 * The player has 20 health parts so that 10 healths will give him
 * half of the total amount of his health shown on the bottom left bar.
 *
 * The food must be the same name as {@link CompMaterial}
 */
@RequiredArgsConstructor
enum Food {
	APPLE(2),
	BAKED_POTATO(5),
	BEETROOT(1),
	BEETROOT_SOUP(6),
	BREAD(5),
	CAKE(2),
	CARROT(3),
	CHORUS_FRUIT(4),
	COOKED_CHICKEN(6),
	COOKED_COD(5),
	COOKED_MUTTON(6),
	COOKED_PORKCHOP(8),
	COOKED_RABBIT(5),
	COOKED_SALMON(6),
	COOKIE(2),
	DRIED_KELP(1),
	GOLDEN_APPLE(4),
	ENCHANTED_GOLDEN_APPLE(4),
	GOLDEN_CARROT(6),
	MELON_SLICE(2),
	MUSHROOM_STEW(6),
	POISONOUS_POTATO(2),
	POTATO(1),
	PUFFERFISH(1),
	PUMPKIN_PIE(8),
	RABBIT_STEW(10),
	RAW_BEEF(3),
	RAW_CHICKEN(2),
	COD(2),
	MUTTON(2),
	PORKCHOP(3),
	RABBIT(3),
	SALMON(2),
	ROTTEN_FLESH(4),
	SPIDER_EYE(2),
	COOKED_BEEF(8),
	SUSPICIOUS_STEW(6),
	SWEET_BERRIES(2),
	TROPICAL_FISH(1);

	/**
	 * How much health to give from this food?
	 */
	@Getter
	private final int health;

	/**
	 * Attempts to find the food item for the particular item stack
	 *
	 * @param item
	 * @return the item, or null if none
	 */
	public static Food getFood(final ItemStack item) {
		final CompMaterial material = CompMaterial.fromMaterial(item.getType());

		return ReflectionUtil.lookupEnumSilent(Food.class, material.toString());
	}
}