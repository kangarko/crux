package org.mineacademy.crux.event;

import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.remain.CompSound;
import org.mineacademy.fo.remain.Remain;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

// NOT suitable for survival servers
public final class JumpBoostListener implements Listener {

	private final static double DOUBLE_JUMP_BOOST = 1.5;
	private final static double PRESSURE_PLATE_BOOST = 2;

	private final static double BOOST_HEIGHT = 1;

	private final Set<UUID> boostedPlayers = new HashSet<>();

	public JumpBoostListener() {
		Common.runTimer(1, new BoostTask());
	}

	@EventHandler
	public void onInteract(final PlayerInteractEvent event) {
		final Block block = event.getClickedBlock();

		if (event.getAction() == Action.PHYSICAL && block != null && CompMaterial.isWoodPressurePlate(block.getType())) {
			final Player player = event.getPlayer();
			final Location location = player.getLocation();

			// Only enable the pressure plates within a certain region
			//final Region spawnRegion = new Region(new Location(null, 0, 0, 0), new Location(null, 0, 0, 0));

			//if (spawnRegion.isWithin(location)) {
				player.setVelocity(location.getDirection().multiply(PRESSURE_PLATE_BOOST).setY(BOOST_HEIGHT));

				Common.runLater(2, () -> boostedPlayers.add(player.getUniqueId()));
			//}
		}
	}

	@EventHandler
	public void onToggleFlight(final PlayerToggleFlightEvent event) {
		final Player player = event.getPlayer();
		final Location location = player.getLocation();
		final ItemStack chestplate = player.getEquipment().getChestplate();

		if (player.getGameMode() == GameMode.SURVIVAL) {
			event.setCancelled(true);

			player.setAllowFlight(false);
			player.setFlying(false);

			if (!boostedPlayers.contains(player.getUniqueId()) && chestplate != null && chestplate.getType() == CompMaterial.ELYTRA.getMaterial()) {
				player.setVelocity(location.getDirection().multiply(DOUBLE_JUMP_BOOST).setY(BOOST_HEIGHT));

				player.playEffect(player.getLocation(), Effect.ENDER_SIGNAL, null);
				CompSound.ENDERDRAGON_WINGS.play(player);

				boostedPlayers.add(player.getUniqueId());
			}
		}
	}

	@EventHandler
	public void onDamage(final EntityDamageEvent event) {
		final Entity entity = event.getEntity();
		final UUID uuid = entity.getUniqueId();

		if (boostedPlayers.contains(uuid) && event.getCause() == EntityDamageEvent.DamageCause.FALL) {
			final float fallDistance = entity.getFallDistance();

			if (fallDistance > 10) {
				Common.log("Damage = " + event.getDamage());
				event.setDamage(event.getDamage() - (fallDistance / 2));

			} else
				event.setCancelled(true);

			boostedPlayers.remove(uuid);
		}
	}

	public class BoostTask extends BukkitRunnable {
		@Override
		public void run() {
			for (final Player player : Remain.getOnlinePlayers()) {
				if (player.getGameMode() != GameMode.SURVIVAL)
					continue;

				final boolean isFalling = player.getVelocity().getY() < -0.1;

				if (player.isOnGround()) {
					player.setAllowFlight(true);

					boostedPlayers.remove(player.getUniqueId());
				}

				else if (isFalling) {
					player.setAllowFlight(false);

					//Common.log("Preventing flight mode");
				}
			}
		}
	}
}
