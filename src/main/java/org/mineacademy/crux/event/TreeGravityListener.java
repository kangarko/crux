package org.mineacademy.crux.event;

import java.util.Collections;
import java.util.List;

import org.bukkit.GameMode;
import org.bukkit.block.Block;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.mineacademy.fo.BlockUtil;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.remain.Remain;

public final class TreeGravityListener implements Listener {

	@EventHandler
	public void onBlockBreak(final BlockBreakEvent event) {
		final Block block = event.getBlock();
		final Player player = event.getPlayer();

		if (!BlockUtil.isLogOnGround(block) || player.getGameMode() != GameMode.SURVIVAL)
			return;

		final List<Block> treeParts = BlockUtil.getTreePartsUp(block);

		// Use this for real tree fall physics
		//Collections.sort(treeParts, (first, second) -> Integer.compare(second.getY(), first.getY()));

		Collections.sort(treeParts, (first, second) -> Integer.compare(first.getY(), second.getY()));

		for (final Block treePart : treeParts)
			// Disable the if line for real tree physics
			if (treePart.getX() == block.getX() && treePart.getZ() == block.getZ() && CompMaterial.isLog(treePart.getType())) {
				final FallingBlock fallingBlock = Remain.spawnFallingBlock(treePart);

				if (CompMaterial.isLeaves(treePart.getType()))
					fallingBlock.setDropItem(false);

				// Use this for real tree physics
				//final Vector direction = player.getLocation().getDirection();
				//final double speedMultiplier = MathUtil.range((treePart.getY() - block.getY()) / 30.0, 0.1, 1.5);
				//fallingBlock.setVelocity(direction.multiply(speedMultiplier));

				treePart.setType(CompMaterial.AIR.getMaterial());
			}
	}
}
