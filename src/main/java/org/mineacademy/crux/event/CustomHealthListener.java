package org.mineacademy.crux.event;

import lombok.Data;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.collection.StrictMap;
import org.mineacademy.fo.remain.CompAttribute;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public final class CustomHealthListener implements Listener {

	private final StrictMap<String, Integer /*20*/> worldHealthMap = new StrictMap<>();
	// TIP: If you want to store this into a file so it does not get lost when you reload or stop the server,
	// you can watch Leaderboards video in Week 3 about PlayerCache and its serialization
	// to create a Set<WorldHealth> in your PlayerCache and make WorldHealth serializable
	private final StrictMap<UUID, Set<WorldHealth>> playerHealthMap = new StrictMap<>();

	public CustomHealthListener() {
		worldHealthMap.put("world", 40);
		worldHealthMap.put("nature", 10);
	}

	@EventHandler
	public void onJoin(final PlayerJoinEvent event) {
		adjustHealth(event.getPlayer());
	}

	@EventHandler
	public void onWorldChange(final PlayerChangedWorldEvent event) {

		final Player player = event.getPlayer();
		final Set<WorldHealth> worldHealthSet = playerHealthMap.getOrPut(player.getUniqueId(), new HashSet<>());

		worldHealthSet.add(new WorldHealth(event.getFrom().getName(), player.getHealth()));
		Common.log("Storing health to " + player.getHealth());

		adjustHealth(event.getPlayer());

		for (final WorldHealth worldHealth : worldHealthSet)
			if (worldHealth.getName().equals(player.getWorld().getName())) {
				worldHealthSet.remove(worldHealth);

				Common.log("Setting new health to " + worldHealth.getHealth());

				player.setHealth(worldHealth.getHealth());
				break;
			}
	}

	private void adjustHealth(final Player player) {
		final Integer health = worldHealthMap.get(player.getWorld().getName());

		if (health != null)
			CompAttribute.GENERIC_MAX_HEALTH.set(player, health);
	}
}

@Data
class WorldHealth {
	private final String name;
	private final double health;
}