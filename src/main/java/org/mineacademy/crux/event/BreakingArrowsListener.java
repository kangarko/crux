package org.mineacademy.crux.event;

import org.bukkit.block.Block;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.remain.CompSound;

public class BreakingArrowsListener implements Listener {

	@EventHandler
	public void onProjectileHit(final ProjectileHitEvent event) {
		final Projectile projectile = event.getEntity();
		final Block hitBlock = event.getHitBlock();

		if (projectile instanceof Arrow && projectile.getShooter() instanceof Player
				&& hitBlock != null && hitBlock.getType().toString().contains("GLASS")) {
			hitBlock.setType(CompMaterial.AIR.getMaterial());
			CompSound.GLASS.play(hitBlock.getLocation());

			projectile.remove();
		}
	}
}
