package org.mineacademy.crux.task;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.mineacademy.fo.remain.Remain;

public final class ElytraTask extends BukkitRunnable {

	private static final int MAXIMUM_BOOST_PITCH = -25;
	private static final double SPEED_MULTIPLIER = 2.5;

	@Override
	public void run() {
		for (final Player player : Remain.getOnlinePlayers()) {
			final Location location = player.getLocation();

			if (player.isGliding() && location.getPitch() < MAXIMUM_BOOST_PITCH)
				player.setVelocity(location.getDirection().multiply(SPEED_MULTIPLIER));
		}
	}
}
